# Endpoint Manage User

---

<a name="section-1"></a>
## Show All User

URL Params
```json
None
```

| ENDPOINT                      | Method | Description                                  | Data                                                                                             |
| ------------------------- | ------ | -------------------------------------------- | ------------------------------------------------------------------------------------------------ |
| /api/v1/user           | GET   | used for show all user        | __Header Request__ :<br>Authorization: string                                  |                                               |


Example :
```bash
www.example.dev/api/v1/user
```
Data Response 

```json
{
    "meta": {
        "code": 200,
        "status": "success",
        "message": "Data User Berhasil Diambil"
    },
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 3,
                "name": "user",
                "email": "user@user.com",
                "email_verified_at": null,
                "photo_user": "null",
                "cover_user": "null",
                "address": "Matesih",
                "gender": "Laki-laki",
                "role": "user",
                "created_at": "2020-10-08T11:08:13.000000Z",
                "updated_at": "2020-10-08T11:08:13.000000Z",
                "contact_user": {
                    "id": 3,
                    "user_id": 3,
                    "whatsapp_number": "-",
                    "link_telegram": "-",
                    "link_instagram": "-",
                    "link_facebook": "-",
                    "created_at": "2020-10-08T11:08:13.000000Z",
                    "updated_at": "2020-10-08T11:08:13.000000Z"
                }
            },
            {
                "id": 4,
                "name": "zainal",
                "email": "zainalarifin080718@gmail.com",
                "email_verified_at": null,
                "photo_user": "null",
                "cover_user": "null",
                "address": "-",
                "gender": "-",
                "role": "user",
                "created_at": "2020-10-08T11:10:55.000000Z",
                "updated_at": "2020-10-08T11:14:51.000000Z",
                "contact_user": {
                    "id": 4,
                    "user_id": 4,
                    "whatsapp_number": "-",
                    "link_telegram": "-",
                    "link_instagram": "-",
                    "link_facebook": "-",
                    "created_at": "2020-10-08T11:10:55.000000Z",
                    "updated_at": "2020-10-08T11:10:55.000000Z"
                }
            },
            {
                "id": 5,
                "name": "ardian",
                "email": "ardian@gmail.com",
                "email_verified_at": null,
                "photo_user": null,
                "cover_user": null,
                "address": "-",
                "gender": "-",
                "role": "user",
                "created_at": "2020-10-08T11:21:30.000000Z",
                "updated_at": "2020-10-08T11:21:30.000000Z",
                "contact_user": {
                    "id": 5,
                    "user_id": 5,
                    "whatsapp_number": "-",
                    "link_telegram": "-",
                    "link_instagram": "-",
                    "link_facebook": "-",
                    "created_at": "2020-10-08T11:23:57.000000Z",
                    "updated_at": "2020-10-08T11:23:57.000000Z"
                }
            }
        ],
        "first_page_url": "http://localhost:8000/api/v1/user?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://localhost:8000/api/v1/user?page=1",
        "links": [
            {
                "url": null,
                "label": "Previous",
                "active": false
            },
            {
                "url": "http://localhost:8000/api/v1/user?page=1",
                "label": 1,
                "active": true
            },
            {
                "url": null,
                "label": "Next",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "http://localhost:8000/api/v1/user",
        "per_page": 10,
        "prev_page_url": null,
        "to": 3,
        "total": 3
    }
}
```


## Delete User

URL Params
```json
None
```

| ENDPOINT                      | Method | Description                                  | Data                                                                                             |
| ------------------------- | ------ | -------------------------------------------- | ------------------------------------------------------------------------------------------------ |
| /api/v1/user/delete/__:id__           | POST   | used for delete user        |  __Header Request__ :<br>Authorization: string                               |                                               |

Example :
```bash
www.example.dev/api/v1/user/delete/1
```

Data Response 

```json
{
    "meta": {
        "code": 200,
        "status": "success",
        "message": "Data User Berhasil Dihapus"
    },
    "data": "0"
}
```
## Reset Account User or Update User Account

URL Params
```json
None
```

| ENDPOINT                      | Method | Description                                  | Data                                                                                             |
| ------------------------- | ------ | -------------------------------------------- | ------------------------------------------------------------------------------------------------ |
| /api/v1/user/update/__:id__           | PUT   | used for update or reset user account        | __Body Request__ :<br>name: String, email: string, password: string, password_confirmation: string <br> __Header Request__ :<br>Authorization: string                                  |                                               |

Example :
```bash
www.example.dev/api/v1/user/update/1
```

Data Response 

```json
{
    "meta": {
        "code": 200,
        "status": "success",
        "message": "Data User Berhasil Diperbarui"
    },
    "data": "0"
}
```


## Show Detail User

URL Params
```json
None
```

| ENDPOINT                      | Method | Description                                  | Data                                                                                             |
| ------------------------- | ------ | -------------------------------------------- | ------------------------------------------------------------------------------------------------ |
| /api/v1/user/__:id__           | GET   | used for show detail user        | __Header Request__ :<br>Authorization: string                                  |                                               |

Example :
```bash
www.example.dev/api/v1/user/3
```

Data Response 

```json
{
    "meta": {
        "code": 200,
        "status": "success",
        "message": "Data Detail User Berhasil diambil"
    },
    "data": {
              "id": 3,
              "name": "user",
              "email": "user@user.com",
              "email_verified_at": null,
              "photo_user": "images/default-image-user/default.jpg",
              "cover_user": "images/default-image-user/default.jpg",
              "address": "Matesih",
              "gender": "Laki-laki",
              "role": "user",
              "created_at": "2020-10-08T11:08:13.000000Z",
              "updated_at": "2020-10-08T11:08:13.000000Z",
              "contact_user": {
                  "id": 3,
                  "user_id": 3,
                  "whatsapp_number": "-",
                  "link_telegram": "-",
                  "link_instagram": "-",
                  "link_facebook": "-",
                  "created_at": "2020-10-08T11:08:13.000000Z",
                  "updated_at": "2020-10-08T11:08:13.000000Z"
           }
}
```
