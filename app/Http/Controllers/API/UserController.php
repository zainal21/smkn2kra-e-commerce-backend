<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;

class UserController extends Controller
{
    public function index($id = false)
    {
        if($id){
            $data = User::with('contact_user')->where(['id' => $id])->first();
            return JSONFormatter::success($data,'Data User Berhasil Diambil');
        }else{
            $data = User::with('contact_user')->paginate(10);
            return JSONFormatter::success($data,'Data User Berhasil Diambil');
        }
    }
    public function destroy($id)
    {
        if($id){
            $data = User::destroy($id);
            return JSONFormatter::success($data,'Data User Berhasil Diambil');
        }else{
            return JSONFormatter::error(null,'Data Tidak Ditemukan', 400);
        }
    }

    public function resetUserAccount(Request $req,$id)
    {
        $validator = Validator::make($req->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
        // jika tidak mempunyai user beberapa sosmed.. dengan memberi tanpa - (tidak boleh kosong)
        if($validator->fails()){
            return JSONFormatter::error($validator->errors(),'Kesalahan input data', 400);
        }
        if($id){
            $data = User::where(['id' => '$id'])->update([
                'name' => $req->name,
                'email' => $req->email,
                'password' => bcrypt($req->password),
                'address' => $req->address,
                'gender' => $req->gender,
                'role' => $req->role,
            ]);
            return JSONFormatter::success($data,'Data User Berhasil Diperbarui');
        }else{
            return JSONFormatter::error(null,'Data Tidak Ditemukan', 400);
        }
    }
}
